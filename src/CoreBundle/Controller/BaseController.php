<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * {@inheritdoc}
 */
class BaseController extends Controller {

    /**
     * add Flash Message
     * supported types:
     *      default     - blue
     *      success     - green
     *      warning     - orange
     *      info        - light blue
     *      alert       - red
     *      secondary   - gray.
     *
     * @param string $message autotranslated, if translation exists
     * @param string $type    = 'default'
     */
    public function addFlashMsg($message, $type = 'default') {
        $this->container
                ->get('session')
                ->getFlashBag()
                ->add($type, $this->trans($message));
    }

    /**
     * Get doctrine.
     * No matter if m*sql or mongo is used.
     *
     * @return mixed
     */
    public function getDoctrine() {
        if ($this->container->has('doctrine_mongodb')) {
            return $this->container->get('doctrine_mongodb');
        }

        return $this->container->get('doctrine');
    }

    /**
     * Get manager.
     * No matter if m*sql or mongo is used.
     *
     * @return mixed
     */
    public function getManager() {

        return $this->getDoctrine()->getManager();
    }

    /**
     * Get repository.
     * No matter if m*sql or mongo is used.
     *
     * @param string $alias
     *
     * @return mixed
     */
    public function getRepo($alias) {
        return $this->getDoctrine()->getManager()->getRepository($alias);
    }

    /**
     * getQb
     * No matter if m*sql or mongo is used.
     *
     * @param string $alias     alias for class
     * @param string $queryName = 'a' queryName for a query
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getQbd($alias, $queryName = 'a') {
        return $this->getDoctrine()
                        ->getRepository($alias)
                        ->createQueryBuilder($queryName);
    }

    /**
     * Translate string.
     *
     * @param string $message translated, if translation exists
     *
     * @return string translated string
     */
    public function trans($message) {
        return $this->container
                        ->get('translator')
                        ->trans($message);
    }

    /**
     * Delete object from database.
     *
     * @param mixed $entity
     *
     * @return boolean
     */
    public function remove($entity) {
        $em = $this->getManager();
        $em->remove($entity);

        return $em->flush();
    }

    /**
     * Persist object in database.
     *
     * @param mixed $entity
     *
     * @return boolean
     */
    public function persist($entity) {
        $em = $this->getManager();
        $em->persist($entity);

        return $em->flush();
    }

    /**
     * Get Pagination - KNPBUNDLE!
     *
     * @param Doctrine\ORM\QueryBuilder $query
     * @param Symfony\Component\HttpFoundation\Request $request
     * @param int $limit
     *
     * @return Knp\Component\Pager\Pagination\SlidingPagination 
     */
    public function getPagination($query, $request, $limit = 10) {
        $paginator = $this->get('knp_paginator');

        return $paginator->paginate(
                        $query, $request->query->getInt('page', 1), $limit, array('wrap-queries' => true)
        );
    }

    public function slug($object, $alias) {
        return $this->get('allset.sluger')->slugify($object, $alias);
    }

    public function settingText($form, $fields = array()) {
        return $this->get('allset.doctrine_helper.settings_value_changer')->valueChangeText($form->all(), $fields);
    }

    public function settingTextarea($form, $fields = array()) {
        return $this->get('allset.doctrine_helper.settings_value_changer')->valueChangeTextarea($form->all(), $fields);
    }

    public function settingBoolean($form, $fields = array()) {
        return $this->get('allset.doctrine_helper.settings_value_changer')->valueChangeBoolean($form->all(), $fields);
    }

    public function settingImage($form, $fields = array()) {
        return $this->get('allset.doctrine_helper.settings_value_changer')->valueChangeImage($form->all(), $fields);
    }

    public function images($object, $form, $field, $fieldName = 'images') {
        $this->get('allset.json_extensions.image_adder')->add($object, $form->get($field)->getData(), $fieldName);
    }

    public function getMenuEntities() {
        $entities = [];
        $entityList = \CMSBundle\Registrant\MenuItems::getEntityList();
        foreach ($entityList as $entity) {

            $repo = $this->getRepo($entity['alias']);
            $entities[$entity['alias']] = [
                'name' => $entity['name'],
                'show_path' => $entity['show_path'],
                'title_field' => $entity['title_filed'],
                'show_path_parameter' => $entity['show_path_parameter'],
                'show_path_parameter_method' => $entity['show_path_parameter_method'],
                'items' => call_user_func([$repo, $entity['repo_method']])
            ];
        }
        return $entities;
    }

    public function renderPage($view, $options = []) {
        $em = $this->getManager();
        $options['settings'] = null;
        $textSettings = $em->getRepository('CMSBundle:TextSetting')->findAll();
        $textareaSettings = $em->getRepository('CMSBundle:TextareaSetting')->findAll();
        $imageSettings = $em->getRepository('CMSBundle:ImageSetting')->findAll();
        $booleanSettings = $em->getRepository('CMSBundle:BooleanSetting')->findAll();
        foreach ($textSettings as $ts) {
            $options['settings']['text'][$ts->getName()] = $ts->getValue();
        }
        foreach ($textareaSettings as $ts) {
            $options['settings']['textarea'][$ts->getName()] = $ts->getValue();
        }
        foreach ($booleanSettings as $bs) {
            $options['settings']['boolean'][$bs->getName()] = $bs->getValue();
        }
        foreach ($imageSettings as $is) {
            foreach ($is->getImages() as $i) {
                $options['settings']['images'][$is->getName()][] = $this->container->get('assets.packages')->getUrl('media/images/' . $i->getImageName());
            }
        }
        $options['menu']['main_menu'] = $this->get('allset.json_extensions.menu_creator')->getMenu('main_menu');

        return $this->render($view, $options);
    }

    public function renderPageView($view, $options = []) {
        $em = $this->getManager();
        $textSettings = $em->getRepository('CMSBundle:TextSetting')->findAll();
        $textareaSettings = $em->getRepository('CMSBundle:TextareaSetting')->findAll();
        $imageSettings = $em->getRepository('CMSBundle:ImageSetting')->findAll();
        $booleanSettings = $em->getRepository('CMSBundle:BooleanSetting')->findAll();
        foreach ($textSettings as $ts) {
            $options['settings']['text'][$ts->getName()] = $ts->getValue();
        }
        foreach ($textareaSettings as $ts) {
            $options['settings']['textarea'][$ts->getName()] = $ts->getValue();
        }
        foreach ($booleanSettings as $bs) {
            $options['settings']['boolean'][$bs->getName()] = $bs->getValue();
        }
        foreach ($imageSettings as $is) {
            foreach ($is->getImages() as $i) {
                $options['settings']['images'][$is->getName()][] = $this->container->get('assets.packages')->getUrl('uploads/page/images/' . $i->getImageName());
            }
        }

        $options['menu']['main_menu'] = $this->get('allset.json_extensions.menu_creator')->getMenu('main_menu');

        return $this->renderView($view, $options);
    }

}
