<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notebook
 *
 * @ORM\Table(name="notebook")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotebookRepository")
 */
class Notebook extends Device
{

    /**
     * @var string|null
     *
     * @ORM\Column(name="gCard", type="string", length=100, nullable=true)
     */
    private $gCard;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="tPad", type="boolean", nullable=true)
     */
    private $tPad;

    /**
     * @var string
     *
     * @ORM\Column(name="keyboard", type="string", length=50)
     */
    private $keyboard;

    /**
     * Set gCard.
     *
     * @param string|null $gCard
     *
     * @return Notebook
     */
    public function setGCard($gCard = null)
    {
        $this->gCard = $gCard;

        return $this;
    }

    /**
     * Get gCard.
     *
     * @return string|null
     */
    public function getGCard()
    {
        return $this->gCard;
    }

    /**
     * Set tPad.
     *
     * @param bool|null $tPad
     *
     * @return Notebook
     */
    public function setTPad($tPad = null)
    {
        $this->tPad = $tPad;

        return $this;
    }

    /**
     * Get tPad.
     *
     * @return bool|null
     */
    public function getTPad()
    {
        return $this->tPad;
    }

    /**
     * Set keyboard.
     *
     * @param string $keyboard
     *
     * @return Notebook
     */
    public function setKeyboard($keyboard)
    {
        $this->keyboard = $keyboard;

        return $this;
    }

    /**
     * Get keyboard.
     *
     * @return string
     */
    public function getKeyboard()
    {
        return $this->keyboard;
    }
}
