<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("email")
 */
class User extends BaseUser {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * One User has Many clientNotifications.
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="client")
     */
    private $clientNotifications;

    /**
     * One User has Many employeeNotifications.
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="employee")
     */
    private $employeeNotifications;

    public function __construct() {
        parent::__construct ();
        $this->clientNotifications = new ArrayCollection();
        $this->employeeNotifications = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return User
     */
    public function setPosition($position = null) {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string|null
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * Set phone.
     *
     * @param string|null $phone
     *
     * @return User
     */
    public function setPhone($phone = null) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone.
     *
     * @return string|null
     */
    public function getPhone() {
        return $this->phone;
    }


  

    /**
     * Add clientNotification.
     *
     * @param \AppBundle\Entity\Notification $clientNotification
     *
     * @return User
     */
    public function addClientNotification(\AppBundle\Entity\Notification $clientNotification)
    {
        $this->clientNotifications[] = $clientNotification;

        return $this;
    }

    /**
     * Remove clientNotification.
     *
     * @param \AppBundle\Entity\Notification $clientNotification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeClientNotification(\AppBundle\Entity\Notification $clientNotification)
    {
        return $this->clientNotifications->removeElement($clientNotification);
    }

    /**
     * Get clientNotifications.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientNotifications()
    {
        return $this->clientNotifications;
    }

    /**
     * Add employeeNotification.
     *
     * @param \AppBundle\Entity\Notification $employeeNotification
     *
     * @return User
     */
    public function addEmployeeNotification(\AppBundle\Entity\Notification $employeeNotification)
    {
        $this->employeeNotifications[] = $employeeNotification;

        return $this;
    }

    /**
     * Remove employeeNotification.
     *
     * @param \AppBundle\Entity\Notification $employeeNotification
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmployeeNotification(\AppBundle\Entity\Notification $employeeNotification)
    {
        return $this->employeeNotifications->removeElement($employeeNotification);
    }

    /**
     * Get employeeNotifications.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployeeNotifications()
    {
        return $this->employeeNotifications;
    }
}
