<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mobile
 *
 * @ORM\Table(name="mobile")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MobileRepository")
 */
class Mobile extends Device
{

    /**
     * @var string|null
     *
     * @ORM\Column(name="bCamera", type="string", length=50, nullable=true)
     */
    private $bCamera;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tSensitivity", type="string", length=50, nullable=true)
     */
    private $tSensitivity;


    /**
     * Set bCamera.
     *
     * @param string|null $bCamera
     *
     * @return Mobile
     */
    public function setBCamera($bCamera = null)
    {
        $this->bCamera = $bCamera;

        return $this;
    }

    /**
     * Get bCamera.
     *
     * @return string|null
     */
    public function getBCamera()
    {
        return $this->bCamera;
    }

    /**
     * Set tSensitivity.
     *
     * @param string|null $tSensitivity
     *
     * @return Mobile
     */
    public function setTSensitivity($tSensitivity = null)
    {
        $this->tSensitivity = $tSensitivity;

        return $this;
    }

    /**
     * Get tSensitivity.
     *
     * @return string|null
     */
    public function getTSensitivity()
    {
        return $this->tSensitivity;
    }
}
