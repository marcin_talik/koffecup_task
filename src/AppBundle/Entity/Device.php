<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Device
 *
 * @ORM\MappedSuperclass()
 * 
 */
class Device extends \CoreBundle\Entity\Base
{
    /**
     * @var float|null
     *
     * @ORM\Column(name="worth", type="float", nullable=true)
     */
    private $worth;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="repairBefore", type="boolean", nullable=true)
     */
    private $repairBefore;

    /**
     * @var string|null
     *
     * @ORM\Column(name="producer", type="string", length=100, nullable=true)
     */
    private $producer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="string", length=30, nullable=true)
     */
    private $color;

    /**
     * @var float|null
     *
     * @ORM\Column(name="screenSize", type="float", nullable=true)
     */
    private $screenSize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="processor", type="string", length=50, nullable=true)
     */
    private $processor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dimenssions", type="string", length=50, nullable=true)
     */
    private $dimenssions;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fcamera", type="string", length=30, nullable=true)
     */
    private $fcamera;

    /**
     * @var string|null
     *
     * @ORM\Column(name="inputs", type="string", length=255, nullable=true)
     */
    private $inputs;

    /**
     * Set worth.
     *
     * @param float|null $worth
     *
     * @return Device
     */
    public function setWorth($worth = null)
    {
        $this->worth = $worth;

        return $this;
    }

    /**
     * Get worth.
     *
     * @return float|null
     */
    public function getWorth()
    {
        return $this->worth;
    }

    /**
     * Set repairBefore.
     *
     * @param bool|null $repairBefore
     *
     * @return Device
     */
    public function setRepairBefore($repairBefore = null)
    {
        $this->repairBefore = $repairBefore;

        return $this;
    }

    /**
     * Get repairBefore.
     *
     * @return bool|null
     */
    public function getRepairBefore()
    {
        return $this->repairBefore;
    }

    /**
     * Set producer.
     *
     * @param string|null $producer
     *
     * @return Device
     */
    public function setProducer($producer = null)
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer.
     *
     * @return string|null
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * Set color.
     *
     * @param string|null $color
     *
     * @return Device
     */
    public function setColor($color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color.
     *
     * @return string|null
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set screenSize.
     *
     * @param float|null $screenSize
     *
     * @return Device
     */
    public function setScreenSize($screenSize = null)
    {
        $this->screenSize = $screenSize;

        return $this;
    }

    /**
     * Get screenSize.
     *
     * @return float|null
     */
    public function getScreenSize()
    {
        return $this->screenSize;
    }

    /**
     * Set processor.
     *
     * @param string|null $processor
     *
     * @return Device
     */
    public function setProcessor($processor = null)
    {
        $this->processor = $processor;

        return $this;
    }

    /**
     * Get processor.
     *
     * @return string|null
     */
    public function getProcessor()
    {
        return $this->processor;
    }


    /**
     * Set dimenssions.
     *
     * @param string|null $dimenssions
     *
     * @return Device
     */
    public function setDimenssions($dimenssions = null)
    {
        $this->dimenssions = $dimenssions;

        return $this;
    }

    /**
     * Get dimenssions.
     *
     * @return string|null
     */
    public function getDimenssions()
    {
        return $this->dimenssions;
    }

    /**
     * Set fcamera.
     *
     * @param string|null $fcamera
     *
     * @return Device
     */
    public function setFcamera($fcamera = null)
    {
        $this->fcamera = $fcamera;

        return $this;
    }

    /**
     * Get fcamera.
     *
     * @return string|null
     */
    public function getFcamera()
    {
        return $this->fcamera;
    }

    /**
     * Set inputs.
     *
     * @param string|null $inputs
     *
     * @return Device
     */
    public function setInputs($inputs = null)
    {
        $this->inputs = $inputs;

        return $this;
    }

    /**
     * Get inputs.
     *
     * @return string|null
     */
    public function getInputs()
    {
        return $this->inputs;
    }
}
