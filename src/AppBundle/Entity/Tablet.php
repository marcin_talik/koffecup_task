<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tablet
 *
 * @ORM\Table(name="tablet")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TabletRepository")
 */
class Tablet extends Device
{

    /**
     * @var string|null
     *
     * @ORM\Column(name="bCamera", type="string", length=50, nullable=true)
     */
    private $bCamera;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tSensitivity", type="string", length=50, nullable=true)
     */
    private $tSensitivity;


    /**
     * Set bCamera.
     *
     * @param string|null $bCamera
     *
     * @return Tablet
     */
    public function setBCamera($bCamera = null)
    {
        $this->bCamera = $bCamera;

        return $this;
    }

    /**
     * Get bCamera.
     *
     * @return string|null
     */
    public function getBCamera()
    {
        return $this->bCamera;
    }



    /**
     * Set tSensitivity.
     *
     * @param string|null $tSensitivity
     *
     * @return Tablet
     */
    public function setTSensitivity($tSensitivity = null)
    {
        $this->tSensitivity = $tSensitivity;

        return $this;
    }

    /**
     * Get tSensitivity.
     *
     * @return string|null
     */
    public function getTSensitivity()
    {
        return $this->tSensitivity;
    }
}
