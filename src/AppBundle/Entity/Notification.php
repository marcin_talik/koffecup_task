<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationRepository")
 */
class Notification extends \CoreBundle\Entity\Base {

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable = true)
     */
    private $status = 'New';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Many Notifications have One Client.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="clientNotifications")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * Many Notifications have One Employee.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="employeeNotifications")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id")
     */
    private $employee;

    /**
     * One Notification has One Notebook.
     * @ORM\OneToOne(targetEntity="Notebook",cascade={"persist"})
     * @ORM\JoinColumn(name="notebook_id", referencedColumnName="id")
     */
    private $notebook;

    /**
     * One Notification has One Tablet.
     * @ORM\OneToOne(targetEntity="Tablet",cascade={"persist"})
     * @ORM\JoinColumn(name="tablet_id", referencedColumnName="id")
     */
    private $tablet;

    /**
     * One Notification has One Mobile.
     * @ORM\OneToOne(targetEntity="Mobile",cascade={"persist"})
     * @ORM\JoinColumn(name="mobile_id", referencedColumnName="id")
     */
    private $mobile;

    /**
     * One Notification has One Address.
     * @ORM\OneToOne(targetEntity="Address",cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Notification
     */
    public function setDescription($description = null) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Notification
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set client.
     *
     * @param \AppBundle\Entity\User|null $client
     *
     * @return Notification
     */
    public function setClient(\AppBundle\Entity\User $client = null) {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Set employee.
     *
     * @param \AppBundle\Entity\User|null $employee
     *
     * @return Notification
     */
    public function setEmployee(\AppBundle\Entity\User $employee = null) {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getEmployee() {
        return $this->employee;
    }

    /**
     * Set notebook.
     *
     * @param \AppBundle\Entity\Notebook|null $notebook
     *
     * @return Notification
     */
    public function setNotebook(\AppBundle\Entity\Notebook $notebook = null) {
        $this->notebook = $notebook;

        return $this;
    }

    /**
     * Get notebook.
     *
     * @return \AppBundle\Entity\Notebook|null
     */
    public function getNotebook() {
        return $this->notebook;
    }

    /**
     * Set tablet.
     *
     * @param \AppBundle\Entity\Tablet|null $tablet
     *
     * @return Notification
     */
    public function setTablet(\AppBundle\Entity\Tablet $tablet = null) {
        $this->tablet = $tablet;

        return $this;
    }

    /**
     * Get tablet.
     *
     * @return \AppBundle\Entity\Tablet|null
     */
    public function getTablet() {
        return $this->tablet;
    }

    /**
     * Set mobile.
     *
     * @param \AppBundle\Entity\Mobile|null $mobile
     *
     * @return Notification
     */
    public function setMobile(\AppBundle\Entity\Mobile $mobile = null) {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile.
     *
     * @return \AppBundle\Entity\Mobile|null
     */
    public function getMobile() {
        return $this->mobile;
    }


    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set address.
     *
     * @param \AppBundle\Entity\Address|null $address
     *
     * @return Notification
     */
    public function setAddress(\AppBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return \AppBundle\Entity\Address|null
     */
    public function getAddress()
    {
        return $this->address;
    }
}
