<?php

namespace AppBundle\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use AppBundle\Entity\User;
use AppBundle\Entity\Notification;
use AppBundle\Entity\Mobile;
use AppBundle\Entity\Tablet;
use AppBundle\Entity\Notebook;
use AppBundle\Entity\Address;

class AppFixtures extends Fixture {

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager) {
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setUsername('emp_' . $i . '');
            $user->setRoles(['ROLE_EMPLOYEE_ADMIN']);
            $user->setEmail('emp_' . $i . '@emp.com');
            $password = $this->encoder->encodePassword($user, 'emp' . $i . '');
            $user->setPassword($password);
            $user->setEnabled(1);
            $manager->persist($user);
        }
        for ($i = 0; $i < 15; $i++) {
            $user = new User();
            $user->setUsername('cli_' . $i . '');
            $user->setRoles(['ROLE_CLIENT']);
            $user->setEmail('cli_' . $i . '@emp.com');
            $user->setEnabled(1);
            $password = $this->encoder->encodePassword($user, 'cli' . $i . '');
            $user->setPassword($password);
            $manager->persist($user);
            for ($a = 0; $a < 4; $a++) {
                $address = new Address;
                $address->setCity('city' . $a);
                $address->setPostalCode('44444' . $a);
                $address->setState('malopolskie');
                $address->setNumber($a);
                $address->setStreet('Street 10' . $a);
                $manager->persist($address);
                $ran = mt_rand(0, 2);
                if ($ran = 0) {
                    $mobile = new Mobile;
                    $mobile->setBCamera('bcamera super');
                    $mobile->setColor('red');
                    $mobile->setDimenssions('5x5x5');
                    $mobile->setFcamera('fcamera super');
                    $mobile->setInputs('have micro USB');
                    $mobile->setProcessor('More powerfull than you think');
                    $mobile->setProducer('Other');
                    $mobile->setRepairBefore(0);
                    $mobile->setScreenSize(7);
                    $mobile->setTSensitivity('good');
                    $mobile->setWorth(500);
                    $manager->persist($mobile);
                } elseif ($ran = 1) {
                    $tablet = new Tablet;
                    $tablet->setBCamera('bcamera super');
                    $tablet->setColor('red');
                    $tablet->setDimenssions('5x5x5');
                    $tablet->setFcamera('fcamera super');
                    $tablet->setInputs('have micro USB');
                    $tablet->setProcessor('More powerfull than you think');
                    $tablet->setProducer('Other');
                    $tablet->setRepairBefore(0);
                    $tablet->setScreenSize(7);
                    $tablet->setTSensitivity('good');
                    $tablet->setWorth(1000);
                    $manager->persist($tablet);
                } else {
                    $notebook = new Notebook;
                    $notebook->setColor('red');
                    $notebook->setDimenssions('5x5x5');
                    $notebook->setFcamera('fcamera super');
                    $notebook->setInputs('have micro USB');
                    $notebook->setProcessor('More powerfull than you think');
                    $notebook->setProducer('Other');
                    $notebook->setRepairBefore(0);
                    $notebook->setScreenSize(7);
                    $notebook->setTPad(1);
                    $notebook->setKeyboard('QWERTY');
                    $notebook->setWorth(1500);
                    $manager->persist($notebook);
                }
                $notification = new Notification();
                $notification->setAddress($address);
                $notification->setTitle('title-' . $a . '-'.$i);
                $notification->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat nisl sed laoreet pharetra. Donec metus ipsum, congue ut metus nec, interdum ultrices lacus. Nam a turpis quis mauris pellentesque lacinia. Morbi molestie scelerisque massa. Nulla viverra felis quis leo porta aliquam. Vestibulum ultricies, nulla nec finibus bibendum, ante turpis aliquam mi, ut volutpat ipsum mauris laoreet eros. Morbi ultrices, nisi et venenatis pellentesque, libero tellus efficitur sapien, a ultrices augue magna mollis dui. Donec massa mauris, aliquet a lacus vel, ornare luctus tellus. Integer ultrices scelerisque sem vel pharetra. Aliquam vitae felis lectus.' . $a);
                $notification->setClient($user);
                if ($ran = 0) {
                    $notification->setMobile($mobile);
                } elseif ($ran = 1) {
                    $notification->setTablet($tablet);
                } else {
                    $notification->setNotebook($notebook);
                }

                $manager->persist($notification);
            }
        }



        $manager->flush();
    }

}
