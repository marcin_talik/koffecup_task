<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;

class NotificationEmployeeType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('status', ChoiceType::class, [
                    'label' => 'Status',
                    'choices' => array(
                        'New' => 'new',
                        'Accepted' => 'accepted',
                        'In progress' => 'in progress',
                        'Ready' => 'ready'
                    ),
                    'expanded' => true,
                ])
                ->add('employee', EntityType::class, array(
                    'class' => User::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->where('u.roles LIKE :roles')
                                ->setParameter('roles', '%"ROLE_EMPLOYEE_ADMIN"%')
                                ->orderBy('u.username', 'ASC');
                    },
                    'choice_label' => 'username',))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'AppBundle_notificationClient';
    }

}
