<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\NotyficationClientType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class NotificationTabletType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', TextType::class, [
                    'label' => 'Title'
                ])
                ->add('tablet', TabletType::class, [
                    'label' => false
                ])
                ->add('address', AddressType::class, [
                    'label' => false,
                ])
                ->add('description', TextareaType::class, [
                    'label' => 'Description',
                    'required' => false,
                    'attr' => array(
                        'rows' => 20
                    )
                ])
              
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'AppBundle_notificationTablet';
    }

}
