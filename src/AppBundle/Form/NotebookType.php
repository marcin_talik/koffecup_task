<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Notebook;
use Symfony\Component\OptionsResolver\OptionsResolver;


class NotebookType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
     
        $builder
                ->add('worth', NumberType::class, [
                    'label' => 'Worth (zl)',
                ])
                ->add('repairBefore', ChoiceType::class, [
                    'label' => 'Repair before?',
                    'choices' => array(
                        'No' => 0,
                        'Yes' => 1
                    )
                ])
                ->add('producer', ChoiceType::class, [
                    'label' => 'Producer',
                    'choices' => array(
                        'Apple' => 'apple',
                        'Samsung' => 'samsung',
                        'Lenovo' => 'lenovo',
                        'Asus' => 'asus',
                        'Sony' => 'sony',
                        'Other' => 'other'
                    ),
                    'expanded' => true,
                ])
                ->add('color', TextType::class, [
                    'label' => 'Color'
                ])
                ->add('screenSize', NumberType::class, [
                    'label' => 'Screen Size (px)'
                ])
                ->add('processor', TextType::class, [
                    'label' => 'Processor'
                ])
                ->add('dimenssions', TextType::class, [
                    'label' => 'Dimenssions (mm)'
                ])
                ->add('fcamera', TextType::class, [
                    'label' => 'Front camera'
                ])
                ->add('inputs', TextType::class, [
                    'label' => 'Inputs'
                ])
                ->add('gCard', TextType::class, [
                    'label' => 'Graphic card'
                ])
                ->add('keyboard', TextType::class, [
                    'label' => 'Keybord type'
                ])
                ->add('tPad', CheckboxType::class, [
                    'label' => 'Touchpad (becouse is so hard to find differences between mobile,notebook and tablet)',
                    'required' => false,
                ])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Notebook::class,
        ));
    }
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'AppBundle_notebook';
    }

}
 