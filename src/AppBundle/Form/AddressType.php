<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Address;

class AddressType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('street', TextType::class, [
                    'label' => 'Street'
                ])
                ->add('number', TextType::class, [
                    'label' => 'Number'
                ])
                ->add('state', ChoiceType::class, [
                    'label' => 'State',
                    'choices' => array(
                        'Dolnośląskie'=>"dolnoslaskie",
                        'Kujawsko-pomorskie'=>"kujawsko-pomorskie",
                        'Lubelskie'=>"lubelskie",
                        'Lubuskie'=>"lubuskie",
                        'Łódzkie'=>"lodzkie",
                        'Małopolskie'=>"malopolskie",
                        'Mazowieckie'=>"mazowieckie",
                        'Opolskie'=>"opolskie",
                        'Podkarpackie'=>"podkarpackie",
                        'Podlaskie'=>"podlaskie",
                        'Pomorskie'=>"pomorskie",
                        'Śląskie'=>"slaskie",
                        'Świętokrzyskie'=>"swietokrzyskie",
                        'Warmińsko-mazurskie'=>"warminsko-mazurskie",
                        'Wielkopolskie'=>"wielkopolskie",
                        'Zachodniopomorskie'=>"zachodniopomorskie"
                    ),
                ])
                ->add('postalCode', TextType::class, [
                    'label' => 'Postal code'
                ])
                ->add('city', TextType::class, [
                    'label' => 'City',
                    'required' => true
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Address::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'AppBundle_address';
    }

}
