<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Notification;
use AppBundle\Form\NotificationClientType;
use AppBundle\Form\NotificationMobileType;
use AppBundle\Form\NotificationNotebookType;
use AppBundle\Form\NotificationTabletType;

/**
 * @Route("/profile/client/notification", name="client_")
 */
class ClientNotificationController extends \CoreBundle\Controller\BaseController {

    /**
     * @Route("/index", name="notification_index")
     */
    public function indexAction(Request $request) {
        $userId = $this->getUser()->getId();
        $query = $this->getRepo('AppBundle:Notification')->findAllByUserId($userId);
        $pagination = $this->getPagination($query, $request, 99999);
        return $this->render('notification/client/index.html.twig', [
                    'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/create", name="notification_create")
     */
    public function createAction(Request $request) {
        if ($request->query->get('device') == null) {
            return $this->render('notification/client/choice.html.twig', [
            ]);
        }
        $notification = new Notification();
        if ($request->query->get('device') == 'tablet') {
            $form = $this->createForm(NotificationTabletType::class, $notification, []);
        } elseif ($request->query->get('device') == 'mobile') {
            $form = $this->createForm(NotificationMobileType::class, $notification, []);
        } elseif ($request->query->get('device') == 'notebook') {
            $form = $this->createForm(NotificationNotebookType::class, $notification, []);
        }
        $form->handleRequest($request);
        if ($form->isValid()) {
            $form->getViewData()->setClient($this->getUser());
            $this->persist($notification);
            $this->addFlashMsg('Dodawanie udane!', 'succes');
            return $this->redirectToRoute('client_notification_index');
        }

        return $this->render('notification/edit.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/notification/edit/{id}", name="notification_edit")
     * @ParamConverter("notification", class="AppBundle:Notification")
     */
    public function editAction(Request $request, Notification $notification) {
        if ($notification->getTablet()) {
            $form = $this->createForm(NotificationTabletType::class, $notification, []);
        } elseif ($notification->getMobile()) {
            $form = $this->createForm(NotificationMobileType::class, $notification, []);
        } elseif ($notification->getNotebook()) {
            $form = $this->createForm(NotificationNotebookType::class, $notification, []);
        }
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->persist($notification);
            $this->addFlashMsg('Edition was so successful!', 'succes');
            return $this->redirectToRoute('client_notification_index');
        }
        return $this->render('notification/edit.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/notification/remove/{id}", name="notification_remove")
     * @ParamConverter("notification", class="AppBundle:Notification")
     */
    public function removeAction(Notification $notification) {
        $this->remove($notification);
        $this->addFlashMsg('Yes, notyfication has been removed!', 'succes');
        return $this->redirectToRoute('client_notification_index');
    }

}
