<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Notification;
use AppBundle\Form\NotificationEmployeeType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/profile/employee/notification", name="employee_")
 */
class EmployeeNotificationController extends \CoreBundle\Controller\BaseController {

    /**
     * @Route("/index", name="notification_index")
     */
    public function indexAction(Request $request) {
        $query = $this->getRepo('AppBundle:Notification')->findAllInDB();
        $pagination = $this->getPagination($query, $request, 9999);
        return $this->render('notification/employee/index.html.twig', [
                    'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/notification/show/{id}", name="notification_show")
     * @ParamConverter("notification", class="AppBundle:Notification")
     */
    public function showAction(Notification $notification) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(0);
// Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($notification) {
            return $notification->getId();
        });
       
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $json = $serializer->serialize($notification, 'json');
        return $this->render('notification/employee/show.html.twig', [
                    'json' => $json,
        ]);
    }

    /**
     * @Route("/notification/edit/{id}", name="notification_edit")
     * @ParamConverter("notification", class="AppBundle:Notification")
     */
    public function editAction(Request $request, Notification $notification) {
        $form = $this->createForm(NotificationEmployeeType::class, $notification, []);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->persist($notification);
            $this->addFlashMsg('Edition was so successful!', 'succes');
            return $this->redirectToRoute('employee_notification_index');
        }
        return $this->render('notification/edit.html.twig', [
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/notification/remove/{id}", name="notification_remove")
     * @ParamConverter("notification", class="AppBundle:Notification")
     */
    public function removeAction(Notification $notification) {
        $this->remove($notification);
        $this->addFlashMsg('Yes, notyfication has been removed!', 'succes');
        return $this->redirectToRoute('employee_notification_index');
    }

}
