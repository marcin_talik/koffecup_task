<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028132234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, street VARCHAR(255) NOT NULL, number VARCHAR(18) NOT NULL, state VARCHAR(50) NOT NULL, postalCode VARCHAR(18) NOT NULL, city VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mobile DROP productionDate');
        $this->addSql('ALTER TABLE notebook DROP productionDate');
        $this->addSql('ALTER TABLE notification ADD address_id INT DEFAULT NULL, ADD title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF5476CAF5B7AF75 ON notification (address_id)');
        $this->addSql('ALTER TABLE tablet DROP productionDate');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAF5B7AF75');
        $this->addSql('DROP TABLE address');
        $this->addSql('ALTER TABLE mobile ADD productionDate DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE notebook ADD productionDate DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_BF5476CAF5B7AF75 ON notification');
        $this->addSql('ALTER TABLE notification DROP address_id, DROP title');
        $this->addSql('ALTER TABLE tablet ADD productionDate DATETIME DEFAULT NULL');
    }
}
