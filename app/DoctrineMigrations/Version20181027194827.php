<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181027194827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mobile (id INT AUTO_INCREMENT NOT NULL, productionDate DATETIME DEFAULT NULL, worth DOUBLE PRECISION DEFAULT NULL, repairBefore TINYINT(1) DEFAULT NULL, producer VARCHAR(100) DEFAULT NULL, color VARCHAR(30) DEFAULT NULL, screenSize DOUBLE PRECISION DEFAULT NULL, processor VARCHAR(50) DEFAULT NULL, name VARCHAR(255) NOT NULL, dimenssions VARCHAR(50) DEFAULT NULL, fcamera VARCHAR(30) DEFAULT NULL, inputs VARCHAR(255) DEFAULT NULL, bCamera VARCHAR(50) DEFAULT NULL, tSensitivity VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notebook (id INT AUTO_INCREMENT NOT NULL, productionDate DATETIME DEFAULT NULL, worth DOUBLE PRECISION DEFAULT NULL, repairBefore TINYINT(1) DEFAULT NULL, producer VARCHAR(100) DEFAULT NULL, color VARCHAR(30) DEFAULT NULL, screenSize DOUBLE PRECISION DEFAULT NULL, processor VARCHAR(50) DEFAULT NULL, name VARCHAR(255) NOT NULL, dimenssions VARCHAR(50) DEFAULT NULL, fcamera VARCHAR(30) DEFAULT NULL, inputs VARCHAR(255) DEFAULT NULL, gCard VARCHAR(100) DEFAULT NULL, tPad TINYINT(1) DEFAULT NULL, keyboard VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, no VARCHAR(30) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_BF5476CA67AA281F (no), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tablet (id INT AUTO_INCREMENT NOT NULL, productionDate DATETIME DEFAULT NULL, worth DOUBLE PRECISION DEFAULT NULL, repairBefore TINYINT(1) DEFAULT NULL, producer VARCHAR(100) DEFAULT NULL, color VARCHAR(30) DEFAULT NULL, screenSize DOUBLE PRECISION DEFAULT NULL, processor VARCHAR(50) DEFAULT NULL, name VARCHAR(255) NOT NULL, dimenssions VARCHAR(50) DEFAULT NULL, fcamera VARCHAR(30) DEFAULT NULL, inputs VARCHAR(255) DEFAULT NULL, bCamera VARCHAR(50) DEFAULT NULL, tSensivity VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mobile');
        $this->addSql('DROP TABLE notebook');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE tablet');
    }
}
