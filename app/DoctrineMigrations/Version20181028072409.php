<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028072409 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification ADD client_id INT DEFAULT NULL, ADD employee_id INT DEFAULT NULL, ADD notebook_id INT DEFAULT NULL, ADD tablet_id INT DEFAULT NULL, ADD mobile_id INT DEFAULT NULL, ADD status VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA19EB6921 FOREIGN KEY (client_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA8C03F15C FOREIGN KEY (employee_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAF74303D6 FOREIGN KEY (notebook_id) REFERENCES notebook (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CA1897676B FOREIGN KEY (tablet_id) REFERENCES tablet (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAB806424B FOREIGN KEY (mobile_id) REFERENCES mobile (id)');
        $this->addSql('CREATE INDEX IDX_BF5476CA19EB6921 ON notification (client_id)');
        $this->addSql('CREATE INDEX IDX_BF5476CA8C03F15C ON notification (employee_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF5476CAF74303D6 ON notification (notebook_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF5476CA1897676B ON notification (tablet_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF5476CAB806424B ON notification (mobile_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA19EB6921');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA8C03F15C');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAF74303D6');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CA1897676B');
        $this->addSql('ALTER TABLE notification DROP FOREIGN KEY FK_BF5476CAB806424B');
        $this->addSql('DROP INDEX IDX_BF5476CA19EB6921 ON notification');
        $this->addSql('DROP INDEX IDX_BF5476CA8C03F15C ON notification');
        $this->addSql('DROP INDEX UNIQ_BF5476CAF74303D6 ON notification');
        $this->addSql('DROP INDEX UNIQ_BF5476CA1897676B ON notification');
        $this->addSql('DROP INDEX UNIQ_BF5476CAB806424B ON notification');
        $this->addSql('ALTER TABLE notification DROP client_id, DROP employee_id, DROP notebook_id, DROP tablet_id, DROP mobile_id, DROP status');
    }
}
